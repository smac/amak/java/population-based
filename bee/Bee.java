package bee;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import eval.Eval;

public class Bee extends BaseAgent {

	BeeEnv env;

	int maxCount;

	Phase phase;
	int stuckCount;

	// EMPLOYED
	List<Double> randomAgent;

	// ONLOOKERS
	double bestFitness;
	// List<Double> randomAgent;

	public Bee(int _id, Eval _eval, BeeEnv _env, int _maxCount) {
		super(_eval, _id);

		env = _env;

		maxCount = _maxCount;

		phase = Phase.EMPLOYED;
		stuckCount = 0;

		vector = generateRandomVector();
		evaluate = this.evaluate(vector);
		fitness = this.fitness(evaluate);
	}

	private void nextPhase() {
		if (phase == Phase.EMPLOYED) {
			phase = Phase.ONLOOKERS;
		} else if (phase == Phase.ONLOOKERS) {
			phase = Phase.SCOUTS;
		} else if (phase == Phase.SCOUTS) {
			phase = Phase.EMPLOYED;
		}
	}

	@Override
	public void perceive() {

		if (phase == Phase.EMPLOYED) {
			randomAgent = env.getRandomAgent(id);
		} else if (phase == Phase.ONLOOKERS) {
			randomAgent = env.getRandomAgent(id);
			bestFitness = env.getBestFitness();
		} else if (phase == Phase.SCOUTS) {
		}

	}

	private boolean randomlyUpdate() {
		int dim = (int) (Math.random() * vector.size());

		List<Double> newVector = new ArrayList<Double>(vector);

		newVector.set(dim, vector.get(dim) + 2 * (Math.random() - 0.5) * (vector.get(dim) - randomAgent.get(dim)));

		boundValue(newVector);

		return compareAndUpdate(newVector);
	}

	private void employedPhase() {
		boolean res = randomlyUpdate();
		if (!res) {
			stuckCount++;
		} else {
			stuckCount = 0;
		}
	}

	private void onlookerPhase() {

		// change p to this ? https://www.hindawi.com/journals/jam/2014/402616/
		double p = (0.9 * fitness / bestFitness) + 0.1;

		if (Math.random() > p) {
			randomlyUpdate();
		}
	}

	private void scoutPhase() {
		if (stuckCount >= maxCount) {
			stuckCount = 0;

			double oldEval = evaluate;
			vector = generateRandomVector();
			evaluate = this.evaluate(vector);
			fitness = this.fitness(evaluate);
			// System.out.println("Reseted : "+oldEval+" to "+evaluate);
		}
	}

	@Override
	public void act() {

		if (phase == Phase.EMPLOYED) {
			employedPhase();
		} else if (phase == Phase.ONLOOKERS) {
			onlookerPhase();
		} else if (phase == Phase.SCOUTS) {
			scoutPhase();
		}
		nextPhase();
	}

	public Phase getPhase() {
		return phase;
	}

}

package eval.fun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class Cigar extends Eval {

	public Cigar() {
		count = new AtomicInteger(0);

		dim = 30;
		objective = (double) 0;
		errorDelta = 0.000_01;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add((double) -10);
			max.add((double) 10);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();
		double result = value.get(0) * value.get(0);

		double other = 0;
		for (int i_dim = 0; i_dim < dim; i_dim++) {
			other += value.get(i_dim) * value.get(i_dim);
		}

		return result + 100_000 * other;
	}
}

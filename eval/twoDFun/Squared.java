package eval.twoDFun;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import eval.Eval;

public class Squared extends Eval {

	public Squared() {
		count = new AtomicInteger(0);

		dim = 2;
		objective = (double) 0;
		errorDelta = 0.000_01;

		min = new ArrayList<Double>();
		max = new ArrayList<Double>();
		for (int i = 0; i < dim; i++) {
			min.add(-500.0);
			max.add(500.0);
		}
	}

	@Override
	public double evaluate(List<Double> value) {
		count.getAndIncrement();

		double x = value.get(0);
		double y = value.get(1);

		double result = Math.abs(x) * Math.abs(y) + Math.abs(x) + Math.abs(y);
		return result;
	}
}
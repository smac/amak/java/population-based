package baseOptiAgent;

import java.util.List;

import eval.Eval;

public abstract class Solver {

	protected String name;

	protected Eval eval;

	protected int maxCycle;
	protected int maxEval;

	public Solver(Eval _eval, int _maxCycle, int _maxEval) {
		eval = _eval;
		maxCycle = _maxCycle;
		maxEval = _maxEval;
	}

	public abstract Result solve();
	// Define name, init env and agents, call findSolution

	protected <A extends BaseAgent, E extends Env> Result findSolution(List<A> agents, E env, int cycleModulo) {
		Result res = new Result(name);

		int cycle = 0;

		double bestEval = env.getBestEval();

		do {
			res.cycle.put(cycle, new Cycle(eval.getCount(), env.getBestEval()));
			for (int i = 0; i < cycleModulo; i++) {
				for (BaseAgent agent : agents) {
					agent.perceive();
				}
				for (BaseAgent agent : agents) {
					agent.decide();
					agent.act();
				}
			}

			double nextEval = env.getBestEval();

			if (nextEval < bestEval) {
				bestEval = nextEval;
			}

			cycle++;

			/*
			 * // Pas à Pas try { System.in.read(); } catch (IOException e) { // TODO
			 * Auto-generated catch block e.printStackTrace(); }
			 */

		} while (Math.abs(env.getBestEval() - eval.getObjective()) > eval.getErrorDelta() && cycle < maxCycle
				&& eval.getCount() < maxEval);

		res.totalCycle = cycle;
		res.totalEval = eval.getCount();
		res.optiFound = Math.abs(env.getBestEval() - eval.getObjective()) <= eval.getErrorDelta();
		return res;
	}

}

package baseOptiAgent;

public class Cycle {

	private int eval;
	private double bestSolution;

	public Cycle(int _eval, double _bestSolution) {
		eval = _eval;
		bestSolution = _bestSolution;
	}

	public double getBestSolution() {
		return bestSolution;
	}

	public int getEval() {
		return eval;
	}

}

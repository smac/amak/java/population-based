package firefly;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import eval.Eval;

public class Firefly extends BaseAgent {

	FAEnv env;

	Phase phase;

	double randomAlpha;

	// CYCLE
	List<List<Double>> agents;
	List<List<Double>> attractiveness;
	List<Double> agentsFit;

	public Firefly(int _id, Eval _eval, FAEnv _env, double _randomAlpha) {
		super(_eval, _id);

		env = _env;

		randomAlpha = _randomAlpha;

		phase = Phase.COMPUTE_DIST;

		vector = generateRandomVector();
		evaluate = this.evaluate(vector);
		fitness = this.fitness(evaluate);
	}

	private void nextPhase() {
		if (phase == Phase.COMPUTE_DIST) {
			phase = Phase.CYCLE;
		} else {
			phase = Phase.COMPUTE_DIST;
		}
	}

	@Override
	public void perceive() {
		if (phase == Phase.COMPUTE_DIST) {
			if (id == 0) {

			}
		} else {
			agents = env.getValues();
			attractiveness = env.getAttractiveness();
			agentsFit = env.getFitness();
		}
	}

	@Override
	public void act() {
		if (phase == Phase.COMPUTE_DIST) {
			if (id == 0) {
				env.computeAttractiveness();
			}
		} else {
			mainAct();
		}
		nextPhase();
	}

	private void mainAct() {
		int update = 0;

		for (int i = 0; i < agents.size(); i++) {

			if (i != id && agentsFit.get(i) > fitness) {

				double att;
				if (i < id) {
					att = attractiveness.get(id).get(i);
				} else {
					att = attractiveness.get(i).get(id);
				}

				List<Double> newvector = new ArrayList<Double>();

				for (int dim = 0; dim < eval.getDim(); dim++) {
					newvector.add(vector.get(dim) // TODO Both variant without (1-att)
							+ att * (agents.get(i).get(dim) - vector.get(dim))
							+ (Math.random() - 0.5) * randomAlpha * (eval.getMax(dim) - eval.getMin(dim)));
					update++;
				}
				this.boundValue(newvector);
				this.compareAndUpdate(newvector);
			}
		}

		if (update == 0) {
			List<Double> newvector = new ArrayList<Double>();

			for (int dim = 0; dim < eval.getDim(); dim++) {
				newvector.add(vector.get(dim) + (Math.random() - 0.5) * randomAlpha);
				update++;
			}
			this.boundValue(newvector);
			this.compareAndUpdate(newvector);
		}

	}
}

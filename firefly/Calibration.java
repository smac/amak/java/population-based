package firefly;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import baseOptiAgent.Cycle;
import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;
import eval.fun.Rastrigin;

public class Calibration {
	public static void main(String[] args) {

		// [PARAM]
		int maxCycle = 200_000;
		int maxEval = 1_000_000;

		double wr = 0;
		double afe = 0;
		double bestSol = 0;

		int nbrTry = 10;

		// [INIT]
		int nbrAgent = 50;
		double bMax = 1; // [0, 1]
		double y = 0.2; // [0.1, 10] théoriquement [0, inf)
		double randomAlpha = 1; // [0, 1]

		for (int i = 0; i < nbrTry; i++) {

			// Eval eval = new StepFunction();
			// Eval eval = new SchwefelFunction1_2();
			// Eval eval = new SchwefelFunction();
			Eval eval = new Rastrigin();
			// Eval eval = new Cigar();
			// Eval eval = new AxisParallelHyper_Ellipsoid();
			// Eval eval = new Beale();

			// [INIT]
			FAEnv env = new FAEnv(bMax, y);

			List<Firefly> agents = new ArrayList<Firefly>();
			for (int i_agent = 0; i_agent < nbrAgent; i_agent++) {
				agents.add(new Firefly(i_agent, eval, env, randomAlpha));
			}
			env.initAgent(agents);

			solve(env, agents, eval, maxCycle, maxEval);

			if (Math.abs(env.getBestEval() - eval.getObjective()) <= eval.getErrorDelta()) {
				wr++;
			}
			afe += eval.getCount();
			bestSol += env.getBestEval();
			System.out.println("done");

		}
		wr = wr / (double) nbrTry;
		afe = afe / (double) nbrTry;
		bestSol = bestSol / (double) nbrTry;
		System.out.println("wr " + wr);
		System.out.println("afe " + afe);
		System.out.println("best sol " + bestSol);

	}

	private static void solve(FAEnv env, List<Firefly> agents, Eval eval, int maxCycle, int maxEval) {
		int cycleModulo = 2;
		int cycle = 0;

		double bestEval = env.getBestEval();

		do {
			for (int i = 0; i < cycleModulo; i++) {
				for (BaseAgent agent : agents) {
					agent.perceive();
				}
				for (BaseAgent agent : agents) {
					agent.decide();
					agent.act();
				}
			}

			double nextEval = env.getBestEval();

			if (nextEval < bestEval) {
				bestEval = nextEval;
			}

			cycle++;
		} while (Math.abs(env.getBestEval() - eval.getObjective()) > eval.getErrorDelta() && cycle < maxCycle
				&& eval.getCount() < maxEval);
	}
}

package smac;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.BaseAgent;
import eval.Eval;
import mas.ui.VUI;
import mas.ui.drawables.DrawableImage;

public class Smagent extends BaseAgent {

	private int nbrAgent;

	private DrawableImage image;

	private double maxDensity;

	private Phase phase;

	private SmacEnv env;

	private int countInactivity;
	private int maxInactivity;

	private List<Double> memoryFit;
	private List<List<Double>> memoryValue;
	private int maxMemory;

	// PHASE CYCLING
	private List<List<Double>> neiValue;
	private List<Double> neiFit;
	private List<Double> neiDensity;
	private List<List<Double>> neiMem;
	private List<Double> neiMemFit;

	private double bestFit;
	private List<Double> bestPos;

	public Smagent(Eval _eval, int _id, SmacEnv _env, int _nbrAgent, double _maxDensity, int _maxInactivity,
			int _maxMemory) {
		super(_eval, _id);

		env = _env;

		maxMemory = _maxMemory;
		memoryFit = new ArrayList<Double>();
		memoryValue = new ArrayList<List<Double>>();

		maxInactivity = _maxInactivity;

		vector = generateRandomVector();
		evaluate = this.evaluate(vector);
		fitness = this.fitness(evaluate);

		image = VUI.get().createImage(vector.get(0), vector.get(1),
		"example/randomants/ressources/ant.png");

		nbrAgent = _nbrAgent;
		maxDensity = _maxDensity;

		phase = Phase.DIST;

		countInactivity = 0;
	}

	@Override
	public void perceive() {
		if (phase == Phase.DIST) {
			if (id == 0) {
				env.computeDistance();
			}
		} else {
			neiValue = env.getNei(id);
			neiFit = env.getFit(id);
			neiDensity = env.getDensity(id);
			neiMem = env.getMemory(id);
			neiMemFit = env.getMemoryFit(id);

			bestFit = env.getBestFitness();
			bestPos = env.getBest();
		}
	}

	@Override
	protected double evaluate(List<Double> vector) {
		double res = eval.evaluate(vector);
		memoryFit.add(fitness(res));
		memoryValue.add(vector);

        Color color1 = Color.RED;
        Color color2 = Color.BLUE;
        double ratio = Math.sqrt(res) / Math.sqrt(251_000);///this.bestFit;
        
        
        int red = (int) (color2.getRed() * ratio + color1.getRed() * (1 - ratio));
        int green = (int) (color2.getGreen() * ratio + color1.getGreen() * (1 - ratio));
        int blue = (int) (color2.getBlue() * ratio + color1.getBlue() * (1 - ratio));
        Color stepColor = new Color(red, green, blue);
		
		VUI.get().createRectangle(vector.get(0), vector.get(1), 1, 1).setColor(stepColor);

		if (memoryFit.size() > maxMemory) {
			memoryFit.remove(0);
			memoryValue.remove(0);
		}

		return res;
	}

	private double attractiveness(double dest) {

		// x = current agent fitness
		// x' = destination fitness
		// xm = max fitness known

		// attractiveness = abs(x' - x) / (best fitness - x)

		return Math.abs(dest - fitness) / (bestFit);
	}

	private List<Double> explorationValue() {

		/*
		 * List<Double> res = new ArrayList<Double>(); for(int dim = 0; dim <
		 * eval.getDim(); dim ++) { res.add((Math.random()-0.5) * 2); } return res;
		 * 
		 */
		if (memoryFit.size() < 2) {
			List<Double> res = new ArrayList<Double>();
			for (int dim = 0; dim < eval.getDim(); dim++) {
				res.add((Math.random() - 0.5) * 2);
			}
			return res;
		}

		List<Double> res = new ArrayList<Double>();
		for (int dim = 0; dim < eval.getDim(); dim++) {
			res.add((double) 0);
		}

		for (int i_mem = 0; i_mem < memoryFit.size(); i_mem++) {
			for (int dim = 0; dim < eval.getDim(); dim++) {
				double addValue = attractiveness(memoryFit.get(i_mem)) * memoryValue.get(i_mem).get(dim);
				if (fitness < memoryFit.get(i_mem)) {
					// Attraction
					res.set(dim, res.get(dim) + addValue);
				} else {
					// repulsion
					res.set(dim, res.get(dim) - addValue);
				}
			}
		}

		for (int i_mem = 0; i_mem < neiMem.size(); i_mem++) {
			for (int dim = 0; dim < eval.getDim(); dim++) {
				double addValue = attractiveness(neiMemFit.get(i_mem)) * neiMem.get(i_mem).get(dim);
				if (fitness < neiMemFit.get(i_mem)) {
					// Attraction
					res.set(dim, res.get(dim) + addValue);
				} else {
					// repulsion
					res.set(dim, res.get(dim) - addValue);
				}
			}
		}

		double dist = 0;
		for (int dim = 0; dim < eval.getDim(); dim++) {
			dist += Math.pow(res.get(dim), 2);
		}
		dist = Math.sqrt(dist);
		for (int dim = 0; dim < eval.getDim(); dim++) {
			res.set(dim, res.get(dim) / dist);
		}

		return res;
	}

	public void actionPhase() {

		int i = 0; // indice of the selected agent to look for

		if ((double) neiValue.size() / (double) nbrAgent < maxDensity) {
			// Low Density

			// find the best nei (max local fitness)
			for (int nextAgent = 0; nextAgent < neiValue.size(); nextAgent++) {
				if (neiFit.get(nextAgent) > neiFit.get(i)) {
					i = nextAgent;
				}
			}

		} else {
			// High Density

			// find the nei with lowest density that have a better fitness
			for (int nextAgent = 0; nextAgent < neiValue.size(); nextAgent++) {
				if (neiDensity.get(nextAgent) < neiDensity.get(i) && neiFit.get(nextAgent) > fitness) {
					i = nextAgent;
				}
			}

		}

		List<Double> randomStep = explorationValue();
		if (neiFit.get(i) <= fitness) { // i'm the best
			List<Double> newvector = new ArrayList<Double>();

			for (int dim = 0; dim < eval.getDim(); dim++) {
				newvector.add(vector.get(dim) + randomStep.get(dim));
			}
			boundValue(newvector); // minValue <= newvector <= maxValue
			if (compareAndUpdate(newvector)) {// Evaluate, update if better
				countInactivity = 0;
			} else {
				countInactivity++;
			}
		} else {
			double att = attractiveness(neiFit.get(i));

			// Step toward him
			List<Double> newvector = new ArrayList<Double>();

			for (int dim = 0; dim < eval.getDim(); dim++) {
				newvector.add(vector.get(dim) // TODO Variant with vector.get(dim) * (1 - attractiveness)
						+ att * (neiValue.get(i).get(dim) - vector.get(dim)) + randomStep.get(dim));
			}
			boundValue(newvector); // minValue <= newvector <= maxValue
			if (compareAndUpdate(newvector)) {// Evaluate, update if better
				countInactivity = 0;
			} else {
				countInactivity++;
			}

			if (countInactivity > maxInactivity) {
				countInactivity = 0;

				// select nei with lowest density -> step toward him.
				if (neiValue.size() != 0) {
					i = 0;
					for (int nextAgent = 1; nextAgent < neiValue.size(); nextAgent++) {
						if (neiDensity.get(nextAgent) < neiDensity.get(i)) {
							i = nextAgent;
						}
					}

					att = attractiveness(neiFit.get(i));
					newvector = new ArrayList<Double>();
					randomStep = explorationValue();

					for (int dim = 0; dim < eval.getDim(); dim++) {
						newvector.add(vector.get(dim) // TODO Variant with vector.get(dim) * (1 - attractiveness)
								+ att * (neiValue.get(i).get(dim) - vector.get(dim)) + randomStep.get(dim));
					}

				} else { // if no nei -> step toward GL
					newvector = new ArrayList<Double>();
					att = attractiveness(bestFit);

					for (int dim = 0; dim < eval.getDim(); dim++) {
						newvector.add(vector.get(dim) // TODO Variant with vector.get(dim) * (1 - attractiveness)
								+ att * (this.bestPos.get(dim) - vector.get(dim)) + randomStep.get(dim));
					}
				}
				boundValue(newvector); // minValue <= newvector <= maxValue
				vector = newvector;
				evaluate = this.evaluate(vector);
				fitness = this.fitness(evaluate);
			}
		}

	}

	private void nextPhase() {
		if (phase == Phase.DIST) {
			phase = Phase.CYCLING;
		} else {
			phase = Phase.DIST;
		}
	}

	@Override
	public void act() {
		if (phase == Phase.DIST) {

		} else {
			actionPhase();
			image.move(vector.get(0), vector.get(1));
			// System.out.println(this);
		}
		nextPhase();
	}

	public List<Double> getMemoryFit() {
		return memoryFit;
	}

	public List<List<Double>> getMemoryValue() {
		return memoryValue;
	}

	@Override
	public String toString() {
		return "[AGENT] pos -> " + this.vector.get(0) + " / " + this.vector.get(1) + "\n" + "Fit -> " + fitness + " / "
				+ " Eval -> " + evaluate;
	}

}

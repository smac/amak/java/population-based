package amakaque;

import java.util.ArrayList;
import java.util.List;

import baseOptiAgent.Result;
import baseOptiAgent.Solver;
import eval.Eval;

public class SMSolver extends Solver {

	public SMSolver(Eval _eval, int _maxCycle, int _maxEval) {
		super(_eval, _maxCycle, _maxEval);
		name = "Spider-Monkey";
	}

	@Override
	public Result solve() {

		// [PARAM]
		int nbrAgent = 50;

		// [0.1 : 0.9]
		double pr = 0.1;

		//
		int maxGroup = 5;

		int localLimit = 1500;
		int globalLimit = 50;

		// [INIT]
		SMEnv env = new SMEnv(localLimit, globalLimit, maxGroup, eval);

		List<SMAgent> agents = new ArrayList<SMAgent>();
		for (int i = 0; i < nbrAgent; i++) {
			agents.add(new SMAgent(i, pr, env, eval));
		}

		Group group = new Group();
		group.addAgents(agents);
		env.initGroup(group);

		return findSolution(agents, env, 6);
	}
}

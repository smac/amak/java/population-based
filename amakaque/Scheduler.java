package amakaque;

import eval.Eval;
import mas.core.ThreeStepCyclable;
import mas.implementation.schedulers.variations.ThreeStepCycling;

public class Scheduler extends ThreeStepCycling {

	public SMEnv env;
	public Eval eval;

	public Scheduler(SMEnv _env, Eval _eval, ThreeStepCyclable... _threeStepCyclables) {
		super(_threeStepCyclables);
		env = _env;
		eval = _eval;
	}

	@Override
	public boolean stopCondition() {
		if (env.getGroups().get(0).getAgents().get(0).getPhase() != Phase.LOCAL_LEADER_PHASE) {
			return false;
		}
		return Math.abs(eval.evaluate(env.getGlobalLeader()) - eval.getObjective()) <= eval.getErrorDelta();
	}
}
